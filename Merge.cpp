#include "Merge.h"
#include<vector>
#include<iostream>
using namespace std;

Merge::Merge(){

}

/**
 * @brief Merge::Merge
 * @param v
 * @param size
 * Faz a ordenação por merge
 */
vector<int> Merge::merge(vector<int> left, vector<int> right)
{
   vector<int> result;
   while ((int)left.size() > 0 || (int)right.size() > 0) {

       this->movementsIncrement();

      if ((int)left.size() > 0 && (int)right.size() > 0) {

          this->comparisonsIncrement();

         if ((int)left.front() <= (int)right.front()) {

             this->comparisonsIncrement();

            result.push_back((int)left.front());
            left.erase(left.begin());
         }
     else {
             this->comparisonsIncrement();
            result.push_back((int)right.front());
            right.erase(right.begin());
         }
      }  else if ((int)left.size() > 0) {
          this->comparisonsIncrement();
            for (int i = 0; i < (int)left.size(); i++)
                  result.push_back(left[i]);
                  this->movementsIncrement();

            break;
      }  else if ((int)right.size() > 0) {
          this->comparisonsIncrement();
            for (int i = 0; i < (int)right.size(); i++)

               result.push_back(right[i]);
               this->movementsIncrement();

            break;
      }
   }
   return result;
}

vector<int> Merge::mergeSort(vector<int> m)
{
   if (m.size() <= 1)
      return m;

   vector<int> left, right, result;
   int middle = ((int)m.size()+ 1) / 2;

   for (int i = 0; i < middle; i++) {
        this->movementsIncrement();
      left.push_back(m[i]);
   }

   for (int i = middle; i < (int)m.size(); i++) {
        this->movementsIncrement();
      right.push_back(m[i]);
   }

   left = mergeSort(left);
   right = mergeSort(right);
   result = merge(left, right);

   return result;
}


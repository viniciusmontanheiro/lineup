#ifndef SORT_H
#define SORT_H

#include<Bubble.h>
#include<Merge.h>
#include<Quick.h>
#include<Selection.h>

class Sort : public Bubble, public Merge, public Selection{
public:
    vector<int> vet;
    Sort();
};

#endif // SORT_H

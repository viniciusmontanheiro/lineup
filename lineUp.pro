#-------------------------------------------------
#
# Project created by QtCreator 2016-05-30T13:31:48
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = lineUp
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Bubble.cpp \
    Quick.cpp \
    Merge.cpp \
    Performace.cpp \
    Insertion.cpp

HEADERS += \
    Bubble.h \
    Quick.h \
    Merge.h \
    Performace.h \
    Insertion.h

OTHER_FILES +=

#ifndef QUICK_H
#define QUICK_H

#include"Performace.h"
#include<vector>
using namespace std;

class Quick : public Performace
{
public:
    vector<int> vet;
    Quick(vector<int>v, int left, int right);
    int partition(vector<int> v, int start, int end);
};

#endif // QUICK_H

#include "Quick.h"
#include<vector>
#include<iostream>
#include <fstream>
#include <algorithm>
using namespace std;

/**
 * @brief Quick::Quick
 * @param v
 * @param left
 * @param right
 * Ordena por quicksort
 */
Quick::Quick(vector<int> v, int start, int end){
    int mid;
    this->vet = v;

    if(start < end){
        this->comparisonsIncrement(); //Incrimenta a comparação
        mid = partition(this->vet,start,end); //Faz a particionamento do vetor
        Quick(this->vet,start,mid-1);// varre os menores
        Quick(this->vet,mid,end); //varre os maiores
    }
    std::sort(this->vet.begin(), this->vet.end());
}

/**
 * @brief Quick::partition
 * @param v
 * @param start
 * @param end
 * Faz o particionamento do vetor e retorna o meio
 * @return
 */
int Quick::partition(vector<int> v, int start, int end){

    this->vet = v;
    int x = this->vet[end];//x e o pivo escolhido aleatoriamente
    int i = start-1;

        for(int j=start; j <= end-1;j++){
            this->movementsIncrement(); // Incrementa o movimento
            if(this->vet[j] <= x){
                 this->comparisonsIncrement();
                i++;
                swap(this->vet[i],v[j]);
            }
        }

        swap(this->vet[i+1],this->vet[end]);
        return i+1;

}




#ifndef PERFORMACE_H
#define PERFORMACE_H

#include<vector>
#include<iostream>
using namespace std;
#include <time.h>

class Performace
{
protected:
    int movements;
    int comparisons;
    double spentTime;
public:
    Performace();
    void movementsIncrement();
    void comparisonsIncrement();
    void spentTimeCalculate(clock_t start,clock_t end);
    void statisticWrite(string title, vector<int>vet);
    void resultShowSaveInFile(string title, vector<int>vet);
};

#endif // PERFORMACE_H

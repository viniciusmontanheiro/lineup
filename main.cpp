#include<Bubble.h>
#include<Quick.h>
#include<Insertion.h>
#include<Merge.h>
#include <termios.h>
#include <stdlib.h>
#include<string>
#include<vector>
#include<time.h>
#include<fstream>
#include<iostream>
using namespace std;

int main(){

    int operacao;
    clock_t start; // Tempo inicio
    clock_t end; // Tempo final
    string line; // Linha atual
    vector<int> v; // Nosso vetor de dados
    fstream file,file2; //Arquivos
    ifstream iFile("file.txt"),iFile2("file2.txt");

    file.open("file.txt",fstream::app|fstream::out); // Criando o primeiro arquivo
    file2.open("file2.txt",fstream::app|fstream::out); // Criando o segundo arquivo

    //Se o arquivo existir e estiver aberto
    if(file.is_open() && file2.is_open()){
        for(int i= 0;i < 1000; i++){
            file << rand() % 100000<<endl; //gerando valores aleatorios para o primeiro arquivo
            file2 << rand() % 100000<<endl;//gerando valores aleatorios para o segundo arquivo
        }
    }

    //Lendo os dados do segundo arquivo
    while (getline(iFile, line)) {
        v.push_back(atoi(line.c_str())); //Adicionando ao vetor
    }

    //Lendo os dados do segundo arquivo
    while (getline(iFile2, line)) {
        v.push_back(atoi(line.c_str())); //Adicionando ao vetor
    }

    while(true){
          cout<<endl;
          cout<<"\nOrdernar terceiro arquivo com:\n"<<endl;
          cout<<" 0 - BUBBLESORT\n"<<endl;
          cout<<" 1 - MERGESORT\n"<<endl;
          cout<<" 2 - QUICKSORT\n"<<endl;
          cout<<" 3 - INSERTIONSORT\n"<<endl;
          cout<<" 4 - SAIR\n"<<endl;
          cout<<": ";
          cin>>operacao;
          cout<<endl;

          switch (operacao){

              case 0:
                {
                    start = clock(); // Pegando o tempo de inicio da execucao

                    Bubble b(v); //Ordenando por bubble
                    end = clock(); // Pegando o tempo de final da execucao

                    b.spentTimeCalculate(start,end); //Cálculando tempo gasto
                    b.resultShowSaveInFile("RESULTADO BUBBLESORT",b.vet); // Imprimindo e salvando valores no arquivo
                    b.statisticWrite("BUBBLESORT INFO",b.vet); //Escrevendo no arquivo de informações

                }

                    break;
              case 1:
                 {
                    start = clock(); //Cálculando tempo gasto
                    Merge m;
                    m.vet = m.mergeSort(v);//Ordenando por merge
                    end = clock(); // Pegando o tempo de final da execucao

                    m.spentTimeCalculate(start,end); //Cálculando tempo gasto
                    m.resultShowSaveInFile("RESULTADO MERGESORT",m.vet); // Imprimindo e salvando valores no arquivo
                    m.statisticWrite("MERGESORT INFO",m.vet); //Escrevendo no arquivo de informações
                 }

                   break;
              case 2:
                 {
                    start = clock(); //Cálculando tempo gasto
                   Quick q(v,0,v.size()); //Ordenando por quick
                   end = clock(); // Pegando o tempo de final da execucao

                   q.spentTimeCalculate(start,end); //Cálculando tempo gasto
                   q.resultShowSaveInFile("RESULTADO QUICKSORT",q.vet); // Imprimindo e salvando valores no arquivo
                   q.statisticWrite("QUICKSORT INFO",q.vet); //Escrevendo no arquivo de informações
                 }

                   break;
              case 3:
                {
                    start = clock(); //Cálculando tempo gasto
                    Insertion i(v); //Ordenando por insertion
                    end = clock();  // Pegando o tempo de final da execucao

                    i.spentTimeCalculate(start,end); //Cálculando tempo gasto
                    i.resultShowSaveInFile("RESULTADO INSERTIONSORT",i.vet); // Imprimindo e salvando valores no arquivo
                    i.statisticWrite("INSERTION INFO",i.vet); //Escrevendo no arquivo de informações
                }

                  break;
              case 4:
                file.close();
                file2.close();
                iFile.close();
                iFile2.close();
                exit(1);
                break;
              default:
                  cout<<"[ERR]-[Opção inválida]";
                  cout<<endl;
                  break;
            }

     }


    return 0;
}



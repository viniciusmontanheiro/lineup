#ifndef MERGE_H
#define MERGE_H

#include"Performace.h";
#include<vector>
using namespace std;

class Merge : public Performace
{

public:
    vector<int> vet;
    Merge();
    vector<int> merge(vector<int> left, vector<int> right);
    vector<int> mergeSort(vector<int> v);
};

#endif // MERGE_H

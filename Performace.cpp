#include "Performace.h"


#include<iostream>
using namespace std;
#include<fstream>
#include<time.h>

Performace::Performace(){
    this->comparisons = 0;
    this->movements = 0;
    this->spentTime = 0;
}

/**
 * @brief Performace::movementsIncrement
 * Incrementa os movimentos
 */
void Performace::movementsIncrement(){
    this->movements++;
}


/**
 * @brief Performace::comparisonsIncrement
 * Incrementa as comprações
 */
void Performace::comparisonsIncrement(){
    this->comparisons++;
}

/**
 * @brief Performace::spentTimeCalculate
 * @param start
 * @param finish
 * obtem o tempo de execução
 */
void Performace::spentTimeCalculate(clock_t start,clock_t end){
    this->spentTime = double(end - start) / CLOCKS_PER_SEC; //Tirando a diferença em segundos
}


/**
 * @brief Performace::statisticWrite
 * @param title
 * Responsável por obter e salvar as informações de execução no arquivo
 * Nome do arquivo : statistics.txt
 */
void Performace::statisticWrite(string title,vector<int> vet){
    fstream file;

    file.open("statistics.txt",fstream::app|fstream::out); // Criando o arquivo de informações

    if(file.is_open()){
        file << "   [" << title << "]   "<<endl;
        file << "[Movimentações] : "<< this->movements<<endl;
        file << "[Comparações] : "<< this->comparisons<<endl;
        file << "[Tempo gasto] : "<< this->spentTime<<" segundos."<<endl<<endl;

    }else{
        cout<<" Não foi possível abrir o arquivo de informações!";
    }
}

/**
   * @brief Merge::resultShowSaveInFile
   * @param title
   * Responsável por imprimir o resultado e salvar no terceiro arquivo os dados ordenados
   * Nome do arquivo : result.txt
*/
void Performace::resultShowSaveInFile(string title,vector<int> vet){
    fstream file;

    file.open("result.txt",fstream::app|fstream::out); // Criando o terceiro arquivo que vai conter o resultado

    cout<<"[" << title <<"]\n";
    for (int i=0; i<vet.size(); i++){
        cout << vet[i]<<endl<<endl;
    }

    cout<< title <<" INFO\n";
    cout << "[Movimentações] : "<< this->movements<<endl;
    cout << "[Comparações] : "<< this->comparisons<<endl;
    cout << "[Tempo gasto]: "<< this->spentTime<<" segundos."<<endl<<endl;

    if(file.is_open()){
         file << " [" << title <<"]"<<endl;
         for (int i=0; i<vet.size(); i++){
             file <<vet[i]<<endl;
         }
    }else{
        cout<<"Não foi possível abrir o arquivo [3] - Resultado!"<<endl;
    }


}




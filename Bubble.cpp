#include "Bubble.h"
#include <vector>
#include<iostream>
using namespace std;

/**
 * @brief Bubble::Bubble
 * @param v
 * @constructor
 * Faz a ordenação por bolha
 */
Bubble::Bubble(vector<int> v){
    this->vet = v;

    for (int i=0;i<this->vet.size()-1;i++){
        for (int j=0;j<this->vet.size()-1;j++){

            this->movementsIncrement();

            if (this->vet[j] > this->vet[j+1]){

                this->comparisonsIncrement();

                swap (this->vet[j],this->vet[j+1]);
            }
        }
    }

}



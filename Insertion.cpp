#include "Insertion.h"
#include <vector>
#include<iostream>
using namespace std;

/**
 * @brief Insertion::Insertion
 * @param v
 * Ordena por inserção
 * @param &v Passamos por endereço para podermos modificar
 */
Insertion::Insertion(vector<int>v){
    this->vet = v;
    int j, val;

    //Varrendo vetor
   for(int i = 1; i < this->vet.size(); i++){

       this->movementsIncrement(); // Incrementando quantidade de movimentos

         val = this->vet[i];

         j = i - 1;

         while(j >= 0 && this->vet[j] > val){

            this->vet[j + 1] = this->vet[j];
            j = j - 1;

             this->comparisonsIncrement(); // Incrementando quantidade de comparações
             this->movementsIncrement(); // Incrementando quantidade de movimentos

         }

         this->vet[j + 1] = val;
      }

}

